## About this Project

We have a list of all the orders to be processed daily which needs to be split by country and assigned to a courier which drives a Van and has a weight limit of 500KG. This list arrives daily in a CSV file and then its necessary show the results in json format.

## Requirements
- Laravel Framework 9.11.0
- MySQL 5.7.37 (using AWS RDS)
- AWS S3 Bucket (to store the files)
- AWS SQS (for the queue system)
- Postman (to test endpoints)
- Mysql Workbench (db model)

## Commands
After clone this project:
- run `composer install` to install all dependencies
- start the server using `php artisan serve`
- on another terminal tab, run `php artisan queue:listen --timeout=0` to listen queues
- to create Client Id and Client secret run `php artisan passport:client --client` then use the Request Access Token inside Postman to generate your token. Use the tab Body to provide the Client id and secret. Then click in Jumia Orders Api on tab Authorization to set the token.
- run the command `php artisan passport:keys` to generate passport keys

## Steps
- Create an endpoint that will receive a .csv file upload
- Process the data and record it in a database
- Validate the file being imported so that only files in .csv format are possible. Check both if its format is csv, and the internal content of the file is really a CSV.
- Limit the amount of requests per minute to prevent flood
- Upload this file to an S3 bucket and generate a processing Queue, to leave this to the server (to avoid the user interrupting the upload halfway and losing data) 
- The Queue must return a flag to store in the DB when the file has already been processed.
- Create an endpoint to show results grouping by weight limit and country code

## Questions and Answers
-What if there is a need to add more items? What if the uploaded CSV was sent the wrong file by mistake? <br>
A: You can replace all data for a given date by uploading a new file and setting the desired date. All data from that date will be erased and replaced with the new ones. (NOTE: The system will prevent you from uploading a new file while another is still being processed.)

## Classes & Controllers
- `app\Classes\AnalyzeFile` contains the methods to import, save and process file
- `app\Classes\DailyOrders` contains the methods to group and provide orders
- `app\Http\Controllers\FileController` and `app\Http\Controllers\OrderController` contains the factory methods to use on endpoints
- `app\Http\Requests\FileProcessRequest` contains the file upload rules
- `app\Jobs\ProcessFile` contains the method to process file on Queue System
- `routes\api` contains routes to endpooints (protected by OAuth - Laravel Passport)

## .env file
- Inside .env file has the configurations about MySQL and AWS (Amazon Web Services)