<?php

namespace App\Classes;

use Illuminate\Support\Facades\DB;
use App\Models\Order;

class DailyOrders {

    /* 
        # Get orders from database by country code
    */
    public function getOrders($country_code) {

        // Select just phone the itens started with $countryCode passed on param
        $orders = Order::where('phone', 'LIKE', "{$country_code}%")->orderBy('id')->get();
        
        // Group orders
        return $this->groupOrders($orders);
    }

    /* 
        # Create group of orders less than 500kg with your ids and weight
        # Return in json format
    */
    public function groupOrders($orders) {
        $weightCounter = 0;
        $weightLimit = 500;
        $finalOrders = [];

        $ordersId = [];
        $totalWeight = 0;

        // Loop orders
        foreach($orders as $order) {

            // Weight counter
            $weightCounter += $order->weight;

            // While weightCounter <= 500, store the orders ids and total weight 
            if($weightCounter <= $weightLimit) {
                $ordersId[] = $order->id;
                $totalWeight = $weightCounter;
            } else {
                // When the weight exceeds 500, store group (previous values) on final array
                array_push($finalOrders, ['order_ids' => $ordersId, 'total_weight' => $totalWeight]);
                $ordersId = [];
                $totalWeight = 0;
                $weightCounter = 0;
            }
        }

        return response()->json(['cargos' => $finalOrders]);
    }
}