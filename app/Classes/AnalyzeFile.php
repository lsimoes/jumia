<?php

namespace App\Classes;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\File;
use App\Models\Order;
use Illuminate\Support\Carbon;

class AnalyzeFile {
    private $file;

    public function __construct () {
        // Invoke class File (Model)
        $this->file = new File();
    }
    /* 
        # Import .csv file do S3 Bucket (AWS) 
        # Use the FileRequest to verify file before upload (type and size)
    */
    public function importFile(Request $request) {

        // Do not allow processing if one is still queued, status = 0
        if(File::where('date', $request['date'])->where('status', 0)->first()) {
            abort(404, 'There is already another file being processed for this date. Try again in a few minutes.');
        }
        else {
            //Variable to allocate the file getted by request
            $file = $request->file('file');

            //Get file attibutes
            $fileOriginalName = $file->getClientOriginalName();
            $fileExtension = $file->getClientOriginalExtension();

            //Create a unique file name
            $fileNewName = Str::uuid();

            //Store file on S3 Bucket
            Storage::disk('s3')->put('orders/'.$fileNewName.'.'.$fileExtension, file_get_contents($file));

            //Register file on database
            $this->registerFile($fileNewName, $request['date']);
        }
    }

    /*  
        # Register file on database to be process by Queue
        # PS: The Queue Process was configured on AWS using Laravel Queue
    */
    public function registerFile($fileName, $date) {

        $create = $this->file->create([
            'uuid' => Str::uuid(),
            'name' => $fileName,
            'status' => 0, // 0 = queuing , 1 = queued
            'date' => $date
        ]);

        // Dispatch Queue
        // Note: use the command php artisan queue:listen on terminal to process queues
        \App\Jobs\ProcessFile::dispatch();
    }

    /* 
        # Processing the csv file, executed by Queue Process
        # 1 - Get one file record with not queued, status 0.
        # 2 - Get and read physical file inside bucket S3
        # 3 - Loop the records
    */
    public function processFile() {
        
        $file = DB::table('files')->where('status', 0)->first();
        
        // Get File
        $csvFile = $this->getTempFile($file->name);
        
        // Serializing csv data
        $ordersObj = $this->csvSerializer($csvFile, $file->date);
        
        // Saving orders
        $this->saveOrders($ordersObj, $file->uuid, $file->date);
    }

    /* 
        Function to convert csv content to object
    */
    public function csvSerializer($file, $date) {
        $orders = [];
        $fileOpened = fopen($file, "r");
        
        while ( ($data = fgetcsv($fileOpened) ) !== FALSE) {
            $orders[] = [
                'uuid' => Str::uuid(),
                'id' => trim($data[0]),
                'email' => trim($data[1]),
                'phone' => trim($data[2]),
                'weight' => trim($data[3]),
                'date' => $date
            ];
        }
        // Removing header names of file
        unset($orders[0]);

        // Return data
        return $orders;
    }
    
    /* Save orders on MySQL DB */
    public function saveOrders($orders, $fileUuid, $date) {

        // Verifying if already have order for this date. If true, delete this orders before
        Order::where('date', $date)->delete();
        
        // Chunk orders to insert 5000 each time
        foreach (array_chunk($orders, 5000) as $oc) {
            Order::insert($oc);
        }

        // Update file status
        File::where('uuid', $fileUuid)->update(['status' => 1]);

        var_dump('File successfully processed!!!');
    }

    /* Function to create temp file to access csv */
    public function getTempFile($fileName) {
        return  Storage::disk('s3')->temporaryUrl('orders/' . $fileName .'.csv', Carbon::now()->addMinute(),
                [
                    'ResponseContentType' => 'application/octet-stream',
                    'ResponseContentDisposition' => 'attachment; filename='.$fileName.'.csv'
                ]);
    }
}