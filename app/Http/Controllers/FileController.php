<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Classes\AnalyzeFile;

use App\Http\Requests\FilePostRequest;

class FileController extends Controller
{
    /* 
        # Factory Methods
        # Invoked by endpoint routes (api)
    */

    //Import File
    public function import(FilePostRequest $request) {
        $af = new AnalyzeFile();
        $af->importFile($request);
    }

    //Process File (for test purpose - endpoint)
    public function process() {
        $pf = new ProcessFile();
        return $pf->processFile();
    }

    
}
