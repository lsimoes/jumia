<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Classes\DailyOrders;


class OrderController extends Controller
{
    /* 
        # Factory Methods
        # Invoked by endpoint routes (api)
    */

    //List Orders
    public function list($country_code) {
        $do = new DailyOrders();
        return $do->getOrders($country_code);
    }
}
