<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Refering Controllers
use App\Http\Controllers\OrderController;
use App\Http\Controllers\FileController;

/* 
    # Creating endpoints routes 
    # Protected by OAuth (Laravel Passport)
*/
Route::middleware('client','throttle:10,1')->post('orders/import-file', [FileController::class, 'import']);
Route::middleware('client','throttle:10,1')->get('orders/list-orders/{country_code}', [OrderController::class, 'list']);
